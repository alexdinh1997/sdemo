package fi.vamk.e1800935.semdemo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SemdemoApplication {

	@Autowired
	private AttendanceRepository repository;
	public static void main(String[] args) {
		SpringApplication.run(SemdemoApplication.class, args);
	}

	@Bean
	public void initDate(){
		Attendance att = new Attendance("QWERTY", new Date(121, 02, 06));
		Attendance att2 = new Attendance("ABC", new Date(121, 01, 01));
		Attendance att3 = new Attendance("XYZ", new Date(121, 12, 31));
		repository.save(att);
		repository.save(att2);
		repository.save(att3);
	}
}
